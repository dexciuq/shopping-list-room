package com.dexciuq.shopping_list_room.common

object Const {
    const val ROOM_DATABASE_NAME = "shopping-list-room.db"
}