package com.dexciuq.shopping_list_room

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShoppingListRoomApplication : Application()